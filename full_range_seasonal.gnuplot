#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.6 patchlevel 6    last modified September 2014
#    	Build System: Linux x86_64
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2014
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set title "Seasonal phenomenon, highlighted with a seasonal gradient in the background"

set border 3
set grid xtics back
set key noreverse enhanced autotitles box linetype -1 linewidth 1.000
set key vert out bottom center
set key width 2 height 1
set rmargin 8

set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %y"
set xtics out nomirror rotate by -45

set ylabel "y" norotate
set ytics out nomirror 1

# Define a color palette representing the "season temperature"
_saturation = .2
set palette maxcolors 4
set palette model HSV defined ( \
  0  200/360. _saturation 1, \
  1  100/360. _saturation 1, \
  2   80/360. _saturation 1, \
  3   20/360. _saturation 1, \
  3    0/360. _saturation 1, \
  3  360/360. _saturation 1, \
  4  290/360. _saturation 1)

set cbrange [0:4]

# Seasons order as in the northern hemiphere
set cbtics offset 0,+3 ( \
  'Winter' 0, \
  'Spring' 1, \
  'Summer' 2, \
  'Autumn' 3, \
  '' 4)

# Utility functions to deal with seasons

# When data is time, intervals are in seconds
one_year = 60 * 60 * 24 * 365.25
three_months = one_year / 4

# Argument is a month between 0 and 11
season(x) = ((int(x) + 1) % 12) / 3

# Argument is a season between 0 and 3.
#
# NOTE: when rotating modulo 12, increasing by 11 is the same as subtracting
# by 1, the increment is used here because gnuplot does not handle the modulo
# with negative numbers very well
first_month_of_season(x) = ((int(x) * 3) + 11) % 12

# Winter starts the previous year if the months are January (0) of February (1)
start_of_season(x) = strptime("%Y-%m-%d", "" . (int(tm_year(x)) - (int(tm_mon(x)) < 2)) . "-" . int(first_month_of_season(season(tm_mon(x))) + 1) . "-21")

# Calculate the difference between two dates.
months_between_dates(a, b) = int((tm_year(b) - tm_year(a)) * 12 + tm_mon(b) - tm_mon(a))

# One season is 3 months.
seasons_between_dates(a, b) = int(months_between_dates(a, b) / 3)

# Dummy plot to gather some stats from the dataset
set terminal unknown
plot 'data.dat' using 1:2

xmin = start_of_season(GPVAL_DATA_X_MIN)
xmax = start_of_season(GPVAL_DATA_X_MAX + three_months)
ymin = GPVAL_Y_MIN
ymax = GPVAL_Y_MAX

set xrange [xmin:xmax]
set yrange [ymin:ymax]

# Calculate the x sampling rate for the '+' plot below.
# Have one sample per season, the +1 is to include the season of xmax.
# This way we draw only the strictly needed boxxys for the background.
x_samples = seasons_between_dates(xmin, xmax) + 1
set samples x_samples, 100

# NOTE that the ticks will be placed at the beginning of months regardless of
# the day of the month of xmin. See:
# https://stackoverflow.com/questions/42212039/gnuplot-xtic-labels-mispositioning-with-dates
set xtics xmin,three_months

set terminal qt 0
#set terminal png notransparent nocrop truecolor rounded enhanced font "arial,8" fontscale 1.0
#set output 'full_range_seasonal.png'

# Use the boxxyerrorbars style to draw the background choosing the color
# according to the season.
plot \
  '+' using (0):(0):(start_of_season($1)):(start_of_season($1 + three_months)):(ymin):(ymax):(season(tm_mon($1))) with boxxy fc palette fs solid notitle, \
  'data.dat' using 1:2 w l lc rgb "black" title "data"
