#!/usr/bin/gnuplot -persist
#
#
#    	G N U P L O T
#    	Version 4.6 patchlevel 6    last modified September 2014
#    	Build System: Linux x86_64
#
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2014
#    	Thomas Williams, Colin Kelley and many others
#
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set title "Basic data plot doesn't highlight the seasonal behaviour"

set border 3
set grid xtics back
set key noreverse enhanced autotitles box linetype -1 linewidth 1.000
set key vert out bottom center
set key width 2 height 1
set rmargin 8

set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %y"
set xtics out nomirror rotate by -45

set ylabel "y" norotate
set ytics out nomirror 1

# When data is time, intervals are in seconds.
one_year = 60 * 60 * 24 * 365.25
three_months = one_year / 4

set xtics three_months

set terminal qt 0
#set terminal png notransparent nocrop truecolor rounded enhanced font "arial,8" fontscale 1.0
#set output 'base.png'

plot \
  'data.dat' using 1:2 w l lc rgb "black" title "data"
