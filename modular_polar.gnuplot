#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.6 patchlevel 6    last modified September 2014
#    	Build System: Linux x86_64
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2014
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set title "Seasonal phenomenon, highlighted using a polar graph"

set xdata time
set timefmt "%Y-%m-%d"
set format x "%s"

# Dummy plot to gather some stats from the dataset
set terminal unknown
plot 'data.dat' using 1:2

xmin = GPVAL_DATA_X_MIN
xmax = GPVAL_DATA_X_MAX
ymin = GPVAL_DATA_Y_MIN
ymax = GPVAL_DATA_Y_MAX

set polar
set border 0
set grid polar back lw 1
set nokey

unset xtics
unset ytics
unset raxis

set size square

# XXX: offsets may need to be adjusted for a different data set
set offsets 0, 0.2, 0.1, 0.1

set rrange [ymin:ymax + 0.5]

# XXX: rtics may need to be adjusted for a different data set
#set rtics ymin,1

year_min = tm_year(xmin)
year_max = tm_year(xmax)

set cbrange [year_min:year_max + 1]

# Generate colorbox labels programmatically
cblabels = 'set cbtics ( '
do for [i=year_min:year_max] {
   cblabels = cblabels . '"' . i .'" ' . i .','
}
cblabels = cblabels . '"" ' . int(year_max + 1) .','
cblabels = cblabels . ' )'
eval(cblabels)

# The value 12 works well with the default colorbox height
cbtics_offset = 12 / (year_max - year_min)
set cbtics offset 0,+cbtics_offset

set palette rgbformulae 31,-11,32
set palette maxcolors (year_max - year_min + 1)

# The radius below places the labels outside of the grid.
#
# XXX: It depends on the magnitude of the data values tho, and it may need to
# be adjusted for a different data set.
r = 1.6
set_label(x, text) = sprintf("set label '%s' at (%f*cos(%f)), (%f*sin(%f)) center", text, r, x, r, x)

# Create labels for the months, apparently gnuplot does not allow to set ttics
month_angle(x) =  x * 2 * pi / 12 + pi / 12
eval set_label(month_angle(0), "Jan")
eval set_label(month_angle(1), "Feb")
eval set_label(month_angle(2), "Mar")
eval set_label(month_angle(3), "Apr")
eval set_label(month_angle(4), "May")
eval set_label(month_angle(5), "Jun")
eval set_label(month_angle(6), "Jul")
eval set_label(month_angle(7), "Aug")
eval set_label(month_angle(8), "Sep")
eval set_label(month_angle(9), "Oct")
eval set_label(month_angle(10), "Nov")
eval set_label(month_angle(11), "Dec")

# Scale the data so that one year covers one circumference,
# this allows samples in the same time of the year to be at the same angles.
one_year = 60 * 60 * 24 * 365.25
scale_date_to_month_polar(x) = (int(x) % int(one_year)) / one_year * (2 * pi)

# With this hacky-ish wrap() function we loose some points when the data wraps
# around, but AFAIK this is unavoidable without preprocessing the data.
year_wrap(old, new) = tm_year(old) == tm_year(new) ? scale_date_to_month_polar(new) : NaN

if (GPVAL_VERSION < 5.0) \
  tcol(x) = timecolumn(x); \
else \
  tcol(x) = timecolumn(x, "%Y-%m-%d")

set terminal qt 0
#set terminal png notransparent nocrop truecolor rounded enhanced font "arial,8" fontscale 1.0
#set output 'modular_polar.png'

saved = -1
plot \
  'data.dat' using (prev=saved, saved=tcol(1), year_wrap(prev, saved)):2:(tm_year(tcol(1))) with lines lc palette lw 1.5
