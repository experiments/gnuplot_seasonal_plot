#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 4.6 patchlevel 6    last modified September 2014
#    	Build System: Linux x86_64
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2014
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set title "Seasonal phenomenon, highlighted with a montly gradient in the background"

set border 3
set grid xtics back
set key noreverse enhanced autotitles box linetype -1 linewidth 1.000
set key vert out bottom center
set key width 2 height 1
set rmargin 8

set xdata time
set timefmt "%Y-%m-%d"
set format x "%b %y"
set xtics out nomirror rotate by -45

set ylabel "y" norotate
set ytics out nomirror 1

# Define a color palette representing the "month temperature".
# Angles taken from the link below, except for the first one as pure cyan was
# too bright:
# http://www.linuxtopia.org/online_books/graphics_tools/gimp_user_manual/images/glossary/colorcircle-en.png
_saturation = .5
set palette maxcolors 12
set palette model HSV defined ( \
  0  220/360. _saturation 1, \
  3  120/360. _saturation 1, \
  5   60/360. _saturation 1, \
  7    0/360. _saturation 1, \
  7  360/360. _saturation 1, \
  9  300/360. _saturation 1, \
  11 240/360. _saturation 1)

set cbrange [0:12]
set cbtics offset 0,+1 ( \
  'Jan'  0, \
  'Feb'  1, \
  'Mar'  2, \
  'Apr'  3, \
  'May'  4, \
  'Jun'  5, \
  'Jul'  6, \
  'Aug'  7, \
  'Sep'  8, \
  'Oct'  9, \
  'Nov' 10, \
  'Dec' 11, \
  ''    12)

# Utility functions to deal with time

# When data is time, intervals are in seconds
one_year = 60 * 60 * 24 * 365.25
one_month = one_year / 12
three_months = one_year / 4

start_of_month(x) = strptime("%Y-%m-%d", "" . int(tm_year(x)) . "-". int(tm_mon(x) + 1) . "-01")

# strptime() seems to adjust the year automatically when passing a month > 12
start_of_next_month(x) = strptime("%Y-%m-%d", "" . int(tm_year(x)) . "-" . (int(tm_mon(x)) + 2) . "-01")

# Calculate the difference between two dates.
months_between_dates(a, b) = int((tm_year(b) - tm_year(a)) * 12 + tm_mon(b) - tm_mon(a))

# Dummy plot to gather some stats from the dataset
set terminal unknown
plot 'data.dat' using 1:2

xmin = start_of_month(GPVAL_DATA_X_MIN)
xmax = start_of_next_month(GPVAL_DATA_X_MAX)
ymin = GPVAL_Y_MIN
ymax = GPVAL_Y_MAX

set xrange [xmin:xmax]
set yrange [ymin:ymax]

# Calculate the x sampling rate for the '+' plot below.
# Have one sample per month, the +1 is to include the month of xmax.
x_samples = months_between_dates(xmin, xmax) + 1
set samples x_samples, 100

set xtics three_months

set terminal qt 0
#set terminal png notransparent nocrop truecolor rounded enhanced font "arial,8" fontscale 1.0
#set output 'full_range_monthly.png'

# The plot is done in three steps:
#
# 1. Plot an undefined function to force gnuplot to use the specified xrange
#    and yrange without rescaling them, this makes sure that changing the
#    sampling interval later does not affect the displayed xrange.
#
# 2. Use the boxxyerrorbars style to draw the background choosing the color
#    according to the month.
#    
#    When doing this, shift samples to make them fall in the middle of months.
#
#    This is needed to avoid "holes" between the boxes because samples are
#    evenly spaced while months lengths are not, so it may happen that
#    a sample crosses the month boundary even when it's not supposed to,
#    resulting in two different samples to draw the same box because of
#    start_of_month() and start_of_next_month().
#
#    Having some "buffering days"  can compensate  for the varying months
#    lengths.
#
# 3. Finally plot the actual data
plot \
  NaN notitle, \
  [xmin - one_month/2 : xmax - one_month/2] '+' using ($1):(0):(start_of_month($1)):(start_of_next_month($1)):(ymin):(ymax):(tm_mon($1)) with boxxy fc palette fs solid notitle, \
  'data.dat' using 1:2 w l lc rgb "black" title "data"
